terraform {
  required_providers {
    hcloud = {
      source  = "hetznercloud/hcloud"
      version = "~> 1.31"
    }
  }
}

provider "hcloud" {
  token = var.hcloud_access_token
}

locals {
  common_tags = {
    terraform = true
  }
  ssh_key_name = format("%s-%s", var.hostname, "terraform-ssh-key")
}

resource "hcloud_ssh_key" "terraform" {
  name       = local.ssh_key_name
  public_key = file(var.ssh_public_key_filepath)
  labels     = local.common_tags
}

resource "hcloud_server" "host01" {
  name        = var.hostname
  image       = var.image
  location    = var.location
  server_type = var.server_type
  ssh_keys = [
    hcloud_ssh_key.terraform.id
  ]
  labels = local.common_tags
}

resource "null_resource" "host01_provisioning" {
  depends_on = [
    hcloud_server.host01
  ]
  triggers = {
    ipv4_address             = hcloud_server.host01.ipv4_address
    ssh_private_key_filepath = var.ssh_private_key_filepath
  }
  connection {
    type        = "ssh"
    host        = self.triggers.ipv4_address
    user        = "root"
    private_key = file(self.triggers.ssh_private_key_filepath)
  }
  provisioner "remote-exec" {
    inline = [
      "export GITLAB_CI_RUNNER_URL=${var.gitlab_ci_runner_url};",
      "export GITLAB_CI_RUNNER_REGISTRATION_TOKEN=${var.gitlab_ci_runner_token};",
      "/opt/scripts/provision.sh;"
    ]
  }
  provisioner "remote-exec" {
    when = destroy
    inline = [
      "/opt/scripts/deprovision.sh"
    ]
  }
}
