variable "hcloud_access_token" {
  type        = string
  description = "The Hetzner cloud access token"
}

variable "ssh_public_key_filepath" {
  type        = string
  description = "The ssh public key file path"
}

variable "ssh_private_key_filepath" {
  type        = string
  description = "The ssh private key file path"
}

variable "image" {
  type        = number
  description = "ID or name of the Image the Server is created from"
}

variable "server_type" {
  type        = string
  description = "The Hetzner cloud server type"
  default     = "cx11"
}

variable "gitlab_ci_runner_url" {
  type        = string
  description = "The Gitlab ci runner url"
  default     = "https://gitlab.com"
}

variable "gitlab_ci_runner_token" {
  type        = string
  description = "The Gitlab ci runner token"
}

variable "hostname" {
  type        = string
  description = "Name of the Server to create (must be unique per project and a valid hostname as per RFC 1123)"
}

variable "location" {
  type        = string
  description = "Location to create the volume in"
  default     = "nbg1"
}
