# hcloud-gitlab-ci-runner-server

This Terraform module helps to create a single virtual server in Hetzner cloud ready to build Gitlab CI jobs.

## Resources

- https://gitlab.com/apilandio/machine-images/gitlab-ci-runner
